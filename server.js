var express = require('express');
var multer = require('multer');
var bodyParser = require('body-parser')
var port = 5010

const storage = multer.diskStorage({
  destination : function(req,file,callback){
    callback(null, './public/upload/');
  },
  filename: function(req,file,callback){
    callback(null, file.fieldname + '-' + Date.now());
  }
});

const fileFilter = function (req, file, callback) {
  // accept image only
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new Error('Only image files are allowed!'), false);
  }
  callback(null, true);
};

const upload = multer({ storage : storage});

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//file upload api 
app.post('/api/upload/single', upload.single('singleFile'), (req,res) => {
  try{
    const file = req.file;
    if (!file) {
      res.status(400).json({
        "status": "failed",
        "code" : "400",
        "message" : "Please upload file"
      });
    }

    res.status(200).json({
      "status": "success",
      "code" : "200",
      "message" : "file uploaded successfully"
    });
  }catch(err){
    console.log(error.message);
    res.status(200).json({
      "status": "failed",
      "code" : "500",
      "message" : error.message
    });
  }
});

//multiple file upload api
app.post('/api/upload/multiple', upload.array('multipleFile',4), (req,res,next) => {
  try{
    const files = req.files;
    if (!files) {
      res.status(400).json({
        "status": "failed",
        "code" : "400",
        "message" : "Please upload file"
      });
    }

    res.status(200).json({
      "status": "success",
      "code" : "200",
      "message" : "file uploaded successfully",
      "data" : files
    });
  }catch(error){
    res.status(200).json({
      "status": "failed",
      "code" : "500",
      "message" : error.message
    });
  }
});


app.listen(port, function() {
	console.log(`Server running on port ${port}`);
});
